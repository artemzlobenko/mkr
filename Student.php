<?php


class Student
{
    private $name;
    private $grade;
    private $GPA;

    /**
     * Student constructor.
     * @param $name
     * @param $grade
     * @param $GPA
     */
    public function __construct($name, $grade, $GPA)
    {
        $this->name = $name;
        $this->grade = $grade;
        $this->GPA = $GPA;
    }



    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getGrade()
    {
        return $this->grade;
    }

    /**
     * @param mixed $grade
     */
    public function setGrade($grade)
    {
        $this->grade = $grade;
    }

    /**
     * @return mixed
     */
    public function getGPA()
    {
        return $this->GPA;
    }

    /**
     * @param mixed $GPA
     */
    public function setGPA($GPA)
    {
        $this->GPA = $GPA;
    }

}
