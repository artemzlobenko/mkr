<?php

include "Student.php";

class School
{
    private $schoolName;
    private $students;

    /**
     * School constructor.
     * @param $schoolName
     */
    public function __construct($schoolName)
    {
        $this->schoolName = $schoolName;
        $this->students = array(
            new Student('Anton', '5th', 9),
            new Student('Stas', '6th', 8.99),
            new Student('Oleg', '5th', 10),
            new Student('Alex', '6th', 7)
        );
    }

    public function getNumberOfStudentsFromGrade($grade)
    {
        $studentCount = 0;
        foreach($this->students as $student)
            if($student->getGrade() == $grade)
                $studentCount++;
        return $studentCount;
    }

    public function getBestStudentFromGrade($grade)
    {
        $studentsFromGrade = array();

        foreach ($this->students as $student)
            if($student->getGrade() == $grade)
                $studentsFromGrade[] = $student;

        $bestStudent = $studentsFromGrade[0];
        foreach($studentsFromGrade as $student)
            if($student->getGPA() > $bestStudent->getGPA())
                $bestStudent = $student;
        return $bestStudent->getName();
    }

    /**
     * @return mixed
     */
    public function getSchoolName()
    {
        return $this->schoolName;
    }

    /**
     * @param mixed $schoolName
     */
    public function setSchoolName($schoolName)
    {
        $this->schoolName = $schoolName;
    }


}
